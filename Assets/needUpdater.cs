﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;
public class needUpdater : MonoBehaviour {

    Button[] buttons;
    CharSettings csObject;
    Array needNames;
	// Use this for initialization
	void Start () {
        buttons = GetComponentsInChildren<Button>();
        csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
        needNames = Enum.GetNames(typeof(Needs));
        UnityEngine.Object[] res = Resources.LoadAll("Down buttons");
    }
	
	// Update is called once per frame
	void Update () {
        for (int i = 0; i < Enum.GetValues(typeof(Needs)).Length; i++)
        {
            if ( csObject.Need[i] <= 20 )
            {
                buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Down buttons/red" + needNames.GetValue(i));
            }
            if (csObject.Need[i] >= 21 && csObject.Need[i]  <= 60)
            {
                buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Down buttons/orange" + needNames.GetValue(i));
            }
            if (csObject.Need[i] >= 61)
            {
                buttons[i].GetComponent<Image>().sprite = Resources.Load<Sprite>("Down buttons/green" + needNames.GetValue(i));
            }
        }
	}
}
