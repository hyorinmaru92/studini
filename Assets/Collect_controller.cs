﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
public class Collect_controller : MonoBehaviour {

    public Text score;
    int _score = 0;
    public bool isSet = false;
    public int _scoreGoal=50;
    public bool isEnd = false;
    public AudioClip scorePointSound;
    public AudioClip explosionSound;
    public AudioClip swallowSound;
    public AudioClip spillSound;
    public Transform explosionAnim;
    public float slowRate = 7f;
    private int timesHalucinated = 0;
    private int timesSlowed = 0;
    void Start()
    {
        score.text = _score.ToString() + "/" + _scoreGoal.ToString();
    }
    void OnTriggerEnter(Collider otherObject)
    {
        if (otherObject.tag == "Notify" )
        {
            score.text = (++_score).ToString() + "/" + _scoreGoal.ToString();
            audio.clip = scorePointSound;
            audio.Play();
        }
        if (otherObject.tag == "Bomb")
        {
            _score -= 5;
            score.text = _score.ToString() + "/" + _scoreGoal.ToString();
            audio.clip = explosionSound;
            Instantiate(explosionAnim, transform.position, Quaternion.identity);
            audio.Play();
        }
        if (otherObject.tag == "Glue")
        {
            if (timesSlowed++ == 0)
            {
                playerController playerController = GameObject.Find("Player").GetComponent<playerController>();
                playerController.speed = playerController.speed / slowRate;
                GameObject.Find("GlueBackground").GetComponent<Animator>().Play("ShowGlue");
            }
            audio.clip = spillSound;
            audio.Play();
            Invoke("StopSlowMovement", 5);
        }
        if (otherObject.tag == "Drug")
        {
            if (timesHalucinated++ == 0)
            {
                playerController playerController = GameObject.Find("Player").GetComponent<playerController>();
                playerController.speed = -playerController.speed;
            }
            audio.clip = swallowSound;
            audio.Play();
            GameObject.Find("Building").GetComponent<Animator>().Play("Drug");
            Invoke("StopHalucination", 5);
            
        }
        if (otherObject.tag != "Wall" && otherObject.tag != "Note")
            Destroy(otherObject.gameObject);

        if (_score == _scoreGoal)
        {
            GameObject.Find("Trash").transform.GetComponent<BoxCollider>().isTrigger = false;
            isEnd = true;
        }
    }
    void StopHalucination()
    {
        if (--timesHalucinated == 0)
        {
            GameObject.Find("Building").GetComponent<Animator>().Play("Empty");
            playerController playerController = GameObject.Find("Player").GetComponent<playerController>();
            playerController.speed = -playerController.speed;
        }
    }
    public void StopSlowMovement()
    {
        if (--timesSlowed == 0)
        {
            playerController playerController = GameObject.Find("Player").GetComponent<playerController>();
            playerController.speed = playerController.speed * slowRate;
            GameObject.Find("GlueBackground").GetComponent<Animator>().Play("HideGlue");
        }
    }
}
