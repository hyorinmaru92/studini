﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class opacity : MonoBehaviour 
{
	private SpriteRenderer render;
	private Color objColor;
	private bool isFading = false;

	void Start () 
	{
		render = GetComponent <SpriteRenderer> ();
		objColor = render.color;
	}

	void Update()
	{
		if( isFading )
		{
			Debug.Log ("fadeOUT");
			Color fade = new Color (255, 255, 255, 0);
			Debug.Log(render.color.ToString());
			render.color = Color.Lerp (render.color, fade, 0.005f*Time.time);
		}
	}

	public void fadeOut()
	{
		isFading = true;
	}

}
