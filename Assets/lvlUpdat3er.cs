﻿using UnityEngine;
using System.Collections;

public class lvlUpdat3er : MonoBehaviour {

    UnityEngine.UI.Image image;
    UnityEngine.UI.Text text;
    CharSettings csObject;
	// Use this for initialization
	void Start () {
        image = GetComponent<UnityEngine.UI.Image>();
        text = GetComponentInChildren<UnityEngine.UI.Text>();
        csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
    }
	
	// Update is called once per frame
	void Update () {
        text.text = csObject.Level.ToString();
        image.fillAmount =(float)(csObject.expToLevel-csObject.FreeExp) / csObject.expToLevel;
	}
}
