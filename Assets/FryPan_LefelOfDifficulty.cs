﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FryPan_LefelOfDifficulty : MonoBehaviour {

	CharSettings csObject;

	public GUISkin DifficultySkin;
	public GUISkin GameOverSkin;
	
	private static float widthButton=200;
	private static float heightButton=100;
	private bool endGame = false;
	private bool exp = true;
	
	private Rect Points =new Rect(Screen.width/2-100,50,350,300);
	private Rect RepeatButton=new Rect(Screen.width-250,600,200,70);
	private Rect OkButton=new Rect(Screen.width-250,700,200,70);
	private Rect TeatherView=new Rect(25,320,350,300);
	public Text  textPoints;
	private Rect easyButton= new Rect(Screen.width/2-widthButton/2,200,widthButton,heightButton);
	private Rect middleButton= new Rect(Screen.width/2-widthButton/2,heightButton+250,widthButton,heightButton);
	private Rect hardButton= new Rect(Screen.width/2-widthButton/2,heightButton*2+300,widthButton,heightButton);
	private Rect normalizedMenuArea;
	private Rect background = new Rect(0,0,Screen.width,Screen.height);
	private bool died=true;
	
	// Use this for initialization
	void Start () {
		normalizedMenuArea = new Rect(0,0,Screen.width,Screen.height);
		csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
	}
	
	void OnGUI() 
	{
		if(died){
			GUI.skin = DifficultySkin;
			GUI.BeginGroup(normalizedMenuArea);
			GUI.Box (background,"Poziom trudności: ");
			if(GUI.Button(new Rect(easyButton), "Łatwy")) {
				FindObjectOfType<load_fry_pan_game> ().setDifficulty(1);
				died=false;
			}
			
			if(GUI.Button(new Rect(middleButton), "Średni")) {
				FindObjectOfType<load_fry_pan_game> ().setDifficulty(2);
				died=false;
			}
			
			if(GUI.Button(new Rect(hardButton), "Trudny")) {
				FindObjectOfType<load_fry_pan_game> ().setDifficulty(3);
				died=false;
			}
			GUI.EndGroup();
		}
		if (endGame) {

			int points;
			if(int.TryParse(textPoints.text,out points))
			{
				if(exp){
					csObject.FreeExp=csObject.FreeExp-points;
					csObject.LevelUp();
					csObject.Need [(int)Needs.hunger]+=points/10;
					if(csObject.Need [(int)Needs.hunger]>100)
						csObject.Need [(int)Needs.hunger]=100;
					exp=false;
				}
			}
			GUI.skin = GameOverSkin;
			GUI.BeginGroup(normalizedMenuArea);
			GUI.Box (background,"");
			GUI.Toggle (TeatherView,false,"");
			GUI.TextArea (Points,textPoints.text+" pkt");
			if(GUI.Button(new Rect(OkButton), "Na dziś koniec.")) {
				Destroy(gameObject);
				Application.LoadLevel("kitchen_scene");
			}
			
			if(GUI.Button(new Rect(RepeatButton), "Bez spiny są drugie terminy...")) {
				Application.LoadLevel("frying_pan_game");
			}
			GUI.EndGroup();
		}
	}
	
	// Update is called once per frame
	void Update () {
		if(FindObjectOfType<FryingPanGameController> ())
			endGame=FindObjectOfType<FryingPanGameController> ().isEnd;
	}
}
