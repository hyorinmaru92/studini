﻿using UnityEngine;
using System.Collections;

public enum state
{
    wake, sleep
}

public class sleepAndWake : MonoBehaviour {

    Animator mainLight;
    Animator backgroundLight;
    Animator model;
    CharSettings csObject;
    state characterState = state.wake;

	// Use this for initialization
	void Start () {
	    mainLight = GameObject.Find("mainLight").GetComponent<Animator>();
        backgroundLight = GameObject.Find("mainBackground").GetComponent<Animator>();
        model = GameObject.Find("model").GetComponent<Animator>();
        csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
		if (csObject.isSleeping)
						Sleep ();
				else
						Wake ();
	}
	
	// Update is called once per frame
    public void toggleWakeOrSleep()
    {
        if (characterState == state.wake)
        {
			Sleep();
        }
        else
        {

            Wake();
            
        }
    }

    void Sleep()
    {
		characterState = state.sleep;
		model.SetBool("sleep", true);
		csObject.isSleeping = true;
        mainLight.Play("darkenMainLamp");
        backgroundLight.Play("darken");
        csObject.Need[(int)Needs.sleep] += 15;
        if (csObject.Need[(int)Needs.sleep] > 100)
            csObject.Need[(int)Needs.sleep] = 100;
    }

    void Wake()
    {
		model.SetBool("sleep", false);
		characterState = state.wake;
		csObject.isSleeping = false;
        mainLight.Play("lightenMainLamp");
        backgroundLight.Play("ligthen");
    }
}
