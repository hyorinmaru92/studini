﻿using UnityEngine;
using System.Collections;

public class charStats : MonoBehaviour {

    CharSettings csObject;
    UnityEngine.UI.Text text;
	// Use this for initialization
	void Start () {
        csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
        text = GetComponent<UnityEngine.UI.Text>();
	}
	
	// Update is called once per frame
	void Update () {
        text.text = csObject.Money + "$";
    }
}
