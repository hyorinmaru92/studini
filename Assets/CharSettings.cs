﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public enum Products
{
    banana,
    burger,
    carrot,
    chicken,
    juice,
    muffin,
    orange,
    pepper,
    strawberry,
    tomato,
};

public enum Needs
{
    hunger,
    alko,
    sleep,
    study,
};

public class CharSettings : MonoBehaviour {

    private int _level;
    private int _freeExp;
    private int _expToLevel;
    private int _money;
	private bool _audio;
    private int[] _needs;
    private int[] _products;
    private float _time;
	private bool _isSleeping;

    void Awake()
    {
        DontDestroyOnLoad(this);
        _needs = new int[Enum.GetValues(typeof(Needs)).Length];
        _products = new int[Enum.GetValues(typeof(Products)).Length];
        _time = Time.time;
        if (PlayerPrefs.HasKey("Level"))
        {
            LoadCharacter();
        }
        else
        {
            _level = 1;
            _freeExp = 1500;
            _expToLevel = 1500;
            _money = 100;
			_audio = true;
			_isSleeping=false;
            for (int i = 0; i < Enum.GetValues(typeof(Needs)).Length; i++)
            {
                    _needs[i] = 50;
            } 
        }
    }

	// Use this for initialization
	void Start () {
      
	}
	
	// Update is called once per frame
	void Update () {
        SaveCharacter();
        if (Time.time - _time > 10)
        {
            for (int i = 0; i < Enum.GetValues(typeof(Needs)).Length; i++)
            {
                if (_needs[i] > 1)
                    _needs[i] -= 1;
            } 
            _time = Time.time;

        }
	}

    public int Level
    {
        get { return _level; }
        set { _level = value; }
    }
    
    public int FreeExp
    {
        get { return _freeExp; }
        set { _freeExp = value; }
    }

    public int Money
    {
        get { return _money; }
        set { _money = value; }
    }

	public bool isSleeping
	{
		get { return _isSleeping; }
		set { _isSleeping = value; }
	}

    public int expToLevel
    {
        get { return _expToLevel; }
        set { _expToLevel = value; }
    }

	public bool Audio
	{
		get { return _audio; }
		set { _audio = value; }
	}

    public int[] Need
    {
        get { return _needs; } 
    }

    public int[] Product
    {
        get { return _products; }
    }

    public void LevelUp()
    {
        if (_freeExp <= 0)
        {
            int expDif = _freeExp;
            _expToLevel = (int)(_expToLevel * 1.15f);
            _freeExp = _expToLevel + expDif;
            _level++;
        }
        else
            return;
    }

    void SaveCharacter()
    {
        PlayerPrefs.SetInt("Level", _level);
        PlayerPrefs.SetInt("Free Exp", _freeExp);
        PlayerPrefs.SetInt("Money", _money);
        PlayerPrefs.SetInt("Exp To Level", _expToLevel);
        PlayerPrefs.SetInt("Audio", _audio ? 1:0);
        for (int i = 0; i < Enum.GetValues(typeof(Needs)).Length; i++)
            PlayerPrefs.SetInt(Enum.GetName(typeof(Needs), i), _needs[i]);
        for (int i = 0; i < Enum.GetValues(typeof(Products)).Length; i++)
            PlayerPrefs.SetInt(Enum.GetName(typeof(Products), i), _products[i]);
    }

    void LoadCharacter()
    {
        _level = PlayerPrefs.GetInt("Level");
        _freeExp = PlayerPrefs.GetInt("Free Exp");
        _money = PlayerPrefs.GetInt("Money");
        _expToLevel = PlayerPrefs.GetInt("Exp To Level");
        _audio = PlayerPrefs.GetInt("Audio") <= 0 ? false: true;
        for (int i = 0; i < Enum.GetValues(typeof(Needs)).Length; i++)
            _needs[i] = PlayerPrefs.GetInt(Enum.GetName(typeof(Needs), i));
        for (int i = 0; i < Enum.GetValues(typeof(Products)).Length; i++)
            _products[i] = PlayerPrefs.GetInt(Enum.GetName(typeof(Products), i));
    }
}
