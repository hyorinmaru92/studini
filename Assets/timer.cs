﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Globalization;

public class timer : MonoBehaviour 
{
	int start;
	public int goal;
	public Text counter;
	private int temp;

	void Start () 
	{
		start = DateTime.Now.Second;
	}

	void Update () 
	{
		int time = DateTime.Now.Second;
		temp = goal - (time - start);
		if (temp >= 60)
			temp -=60;
		string msg = Convert.ToString(temp);

		if(counter)
		counter.text = msg;
	}

	public void setTimer(int time)
	{
		goal += time;
	}

	public void timerStart()
	{
		start = DateTime.Now.Second;
	}

	public int getTime()
	{
		return temp;
	}
}