﻿using UnityEngine;
using System.Collections;

public class checkIfAudioEnabled : MonoBehaviour {

	// Use this for initialization
	void Start () {
        CharSettings settings = GameObject.Find("CharSettings").GetComponent<CharSettings>();
        if (settings.Audio)
            GetComponent<AudioSource>().Play();
	}
	

}
