﻿using UnityEngine;
using System.Collections;

public class playerController : MonoBehaviour {

    public float speed = 1f;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.LoadLevel("university_scene");
    }

	void FixedUpdate () {
        transform.rigidbody.velocity = new Vector3();
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            transform.Translate(touchDeltaPosition.x * speed * Time.deltaTime, 0, 0);
        }
        //float move = Input.acceleration.x * 2;

        //if (move > 1)
        //    move = 1;
        //if (move < -1)
        //    move = -1;

        //Vector3 movement = new Vector3(move * speed * Time.deltaTime, 0f, 0f);
        //transform.rigidbody.velocity = movement;
	}
}
