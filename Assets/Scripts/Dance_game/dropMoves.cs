﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class dropMoves : MonoBehaviour 
{
	private int points = 0;
	private Text count;
	public float interval=1f;
	public float pause = 2f;
    public GameObject audio;
	private bool gameStart=false;
	CharSettings csObject;

	void Start(){
		csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
	}

	void Update () 
	{
        if (gameStart)
        {
			InvokeRepeating("drop", pause, interval);
			if(csObject.Audio)
				audio.gameObject.SetActive(true);
			gameStart=false;
        }	
	}

	public void setDifficultLevel(int level)
	{
		if (level == 1)
			interval = 1.5f;
		else if (level == 2)
			interval = 1.1f;
		else
			interval = 0.7f;
		gameStart=true;
	}

	public void drop()
	{
		int move = Random.Range (0, 4);
		switch (move) 
		{
		case 0:
			Instantiate (Resources.Load ("move_left"), transform.position, transform.rotation);
			break;
		case 1:
			Instantiate (Resources.Load ("move_right"), transform.position, transform.rotation);
			break;
		case 2:
			Instantiate (Resources.Load ("move_up"), transform.position, transform.rotation);
			break;
		case 3:
			Instantiate (Resources.Load ("move_down"), transform.position, transform.rotation);
			break;
		}
	}

}
