﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class button_moves : MonoBehaviour 
{
	public Transform test;
	private check_moves moves;
	public Text score;
	private int points = 0;
	public Sprite green;
	public Sprite red;
	public Sprite gray;
	private SpriteRenderer render;
    private Animator anim;

	void Start()
	{
		moves = test.GetComponent<check_moves> ();
		render = test.GetComponent <SpriteRenderer> ();
        anim = GameObject.Find("model").GetComponent<Animator>();
        anim.Play("Armature|idleDance");
	}
    void FixedUpdate()
    {
        anim.SetBool("danceUP", false);
        anim.SetBool("danceDown", false);
        anim.SetBool("danceRight", false);
        anim.SetBool("danceLeft", false);
    }
	public void swapColorGreen()
	{	
		render.sprite = green;
	}

	public void swapColorRed()
	{	
		render.sprite = red;
	}

	public void swapColorGray()
	{
		render.sprite = gray;
	}

	public void buttonUp()
	{
		if(moves.up)
		{
			points+=5;
			score.text = points.ToString();
			Destroy(moves.up1.gameObject);
			moves.up = false;
			//render.sprite = green;
			Invoke ("swapColorGreen", 0f);
			Invoke ("swapColorGray", 0.5f);
            anim.SetBool("danceUP", true);
		}
		else
		{
			points-=5;
			score.text = points.ToString();
			moves.up = false;
			//render.sprite = red;
			Invoke ("swapColorRed", 0f);
			Invoke ("swapColorGray", 0.5f);
		}
	}

	public void buttonDown()
	{
		if(moves.down)
		{
			points+=5;
			score.text = points.ToString();
			Destroy(moves.down1.gameObject);
			moves.down = false;
			Invoke ("swapColorGreen", 0f);
			Invoke ("swapColorGray", 0.5f);
            anim.SetBool("danceDown", true);
		}
		else
		{
			points-=5;
			score.text = points.ToString();
			moves.down = false;
			Invoke ("swapColorRed", 0f);
			Invoke ("swapColorGray", 0.5f);
		}
	}

	public void buttonLeft()
	{
		if(moves.left)
		{
			points+=5;
			score.text = points.ToString();
			Destroy(moves.left1.gameObject);
			moves.left = false;
			Invoke ("swapColorGreen", 0f);
			Invoke ("swapColorGray", 0.5f);
            anim.SetBool("danceLeft", true);
		}
		else
		{
			points-=5;
			score.text = points.ToString();
			moves.left = false;
			Invoke ("swapColorRed", 0f);
			Invoke ("swapColorGray", 0.5f);
		}
	}

	public void buttonRight()
	{
		if(moves.right)
		{
			points+=5;
			score.text = points.ToString();
			Destroy(moves.right1.gameObject);
			moves.right = false;
			Invoke ("swapColorGreen", 0f);
			Invoke ("swapColorGray", 0.5f);
            anim.SetBool("danceRight", true);
		}
		else
		{
			points-=5;
			score.text = points.ToString();
			moves.right = false;
			Invoke ("swapColorRed", 0f);
			Invoke ("swapColorGray", 0.5f);
		}
	}
}
