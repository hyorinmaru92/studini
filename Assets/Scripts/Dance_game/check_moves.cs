﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class check_moves : MonoBehaviour 
{
	public int points = 0;
	public string tag;
	public Text score;
	public bool up, down, left, right;
	public Transform up1, down1, left1, right1;


	void Start()
	{
		up = down = left = right = false;
	}

	void OnTriggerEnter2D(Collider2D other)
	{
		if(other.gameObject.tag == "Left")
		{
			up = false;
			down = false;
			left = true;
			right = false;
			left1 = other.gameObject.transform;
		}

		if(other.gameObject.tag == "Right")
		{
			up = false;
			down = false;
			left = false;
			right = true;
			right1 = other.gameObject.transform;
		}

		if(other.gameObject.tag == "Up")
		{
			up = true;
			down = false;
			left = false;
			right = false;
			up1 = other.gameObject.transform;
		}

		if(other.gameObject.tag == "Down")
		{
			up = false;
			down = true;
			left = false;
			right = false;
			down1 = other.gameObject.transform;
		}
	}

	void OnTriggerExit2D(Collider2D other)
	{
		if(other.gameObject.tag == "Left")
		{
			left = false;
			left1.GetComponent<opacity>().fadeOut();
		}
		
		if(other.gameObject.tag == "Right")
		{
			right = false;
			right1.GetComponent<opacity>().fadeOut();
		}
		
		if(other.gameObject.tag == "Up")
		{
			up = false;
			up1.GetComponent<opacity>().fadeOut();
		}
		
		if(other.gameObject.tag == "Down")
		{
			down = false;
			down1.GetComponent<opacity>().fadeOut();
		}
	}


}
