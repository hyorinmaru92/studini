﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class drop_balls : MonoBehaviour
{
    public List<Object> prefabs;
    public Object[][] food;
    public bool[][] visited;

    public int difficulty = 0;
    //difficulty minimum 3, max 10
    public bool gameOver = false;
    public bool start = false;
    public int x = 5;
    public int y = 7;

    void Start()
    {
        prefabs = new List<Object>();

        for (int i = 0; i < 10; i++)
        {
            prefabs.Add(Resources.Load("balls"+i));
        }

        food = new Object[x][];
        for (int i = 0; i < food.Length; i++)
        {
            food[i] = new Object[y];
        }

        visited = new bool[x][];
        for (int i = 0; i < visited.Length; i++)
        {
            visited[i] = new bool[y];
        }   
    }

    void Update()
    {
        if (start)
        {
            spawn();
        }
    }

    public void spawn()
    {
        for (int i = 0; i < x; i++)
        {
            for (int j = 0; j < y; j++)
            {
                int prefab = Random.Range(0, difficulty);
                food[i][j] = Instantiate(prefabs[prefab], new Vector3(i * 2, j * 2, 0), Quaternion.identity);
                visited[i][j] = false;
            }
        }
        start = false;    
    }

    public void setDifficultLevel(int lev)
    {
        if (lev == 1)
            difficulty = 3;
        else if (lev == 2)
            difficulty = 6;
        else
            difficulty = 10;
        start = true;
    }
}