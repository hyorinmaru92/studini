﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;
using System;

public class balls_main : MonoBehaviour
{
    Touch touch;
    GameObject temp;
    drop_balls db;
    public int points = 0;

    public UnityEngine.Object[][] tempFood;
    public bool[][] deleted;
    public string thisName;
    public string nextName;

    void Start()
    {
        temp = GameObject.Find("dropPoint");
        db = temp.GetComponent<drop_balls>();

        tempFood = new UnityEngine.Object[db.x][];
        for (int i = 0; i < tempFood.Length; i++)
        {
            tempFood[i] = new UnityEngine.Object[db.y];
        }

        deleted = new bool[db.x][];
        for (int i = 0; i < deleted.Length; i++)
        {
            deleted[i] = new bool[db.y];
        }
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.LoadLevel("kitchen_scene");
        }

        if (Input.touches.Length > 0)
        {
            Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			Vector2 touchPos = new Vector2(wp.x, wp.y);
			if (collider2D == Physics2D.OverlapPoint(touchPos))
			{
				for (int i=0; i< Input.touchCount; i++)
				{
					touch = Input.GetTouch(i);
					find(touch);
				}
				
			}
        }
    }

    public void find(Touch touch)
    {
        if (touch.phase == TouchPhase.Ended)
        {
            RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);
            Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);
            if (hit.collider != null)
            {
                if (hit.collider.gameObject == gameObject)
                {            
                    int x_parameter = 0;
                    int y_parameter = 0;

                    if (Mathf.RoundToInt(transform.position.x) == 2)
                    {
                        x_parameter = 1;
                    }
                    else if (Mathf.RoundToInt(transform.position.x) == 4)
                    {
                        x_parameter = 2;
                    }
                    else if (Mathf.RoundToInt(transform.position.x) == 6)
                    {
                        x_parameter = 3;
                    }
                    else if (Mathf.RoundToInt(transform.position.x) == 8)
                    {
                        x_parameter = 4;
                    }

                    if (Mathf.RoundToInt(transform.position.y) == 1)
                    {
                        y_parameter = 1;
                    }
                    else if (Mathf.RoundToInt(transform.position.y) == 2)
                    {
                        y_parameter = 2;
                    }
                    else if (Mathf.RoundToInt(transform.position.y) == 3)
                    {
                        y_parameter = 3;
                    }
                    else if (Mathf.RoundToInt(transform.position.y) == 5)
                    {
                        y_parameter = 4;
                    }
                    else if (Mathf.RoundToInt(transform.position.y) == 6)
                    {
                        y_parameter = 5;
                    }
                    else if (Mathf.RoundToInt(transform.position.y) == 7)
                    {
                        y_parameter = 6;
                    }

                    clear(x_parameter, y_parameter);
                    setCells();
                }
            }
        }
    }

    void OnMouseDown()
    {
        
    }

    public void clear1(int x, int y)
    {
        RaycastHit2D hit = Physics2D.Raycast(new Vector2(touch.position.x, touch.position.y), Vector2.zero);
        Vector3 pos = new Vector2(touch.position.x, touch.position.y);
        if (hit.collider != null)
        {
            if (hit.collider.gameObject == gameObject)
            {
                int x_parameter = 0;
                int y_parameter = 0;

                if (Mathf.RoundToInt(transform.position.x) == 2)
                {
                    x_parameter = 1;
                }
                else if (Mathf.RoundToInt(transform.position.x) == 4)
                {
                    x_parameter = 2;
                }
                else if (Mathf.RoundToInt(transform.position.x) == 6)
                {
                    x_parameter = 3;
                }
                else if (Mathf.RoundToInt(transform.position.x) == 8)
                {
                    x_parameter = 4;
                }

                if (Mathf.RoundToInt(transform.position.y) == 1)
                {
                    y_parameter = 1;
                }
                else if (Mathf.RoundToInt(transform.position.y) == 2)
                {
                    y_parameter = 2;
                }
                else if (Mathf.RoundToInt(transform.position.y) == 3)
                {
                    y_parameter = 3;
                }
                else if (Mathf.RoundToInt(transform.position.y) == 5)
                {
                    y_parameter = 4;
                }
                else if (Mathf.RoundToInt(transform.position.y) == 6)
                {
                    y_parameter = 5;
                }
                else if (Mathf.RoundToInt(transform.position.y) == 7)
                {
                    y_parameter = 6;
                }

                db.visited[x_parameter][y_parameter] = true;
                Destroy(db.food[x_parameter][y_parameter]);
                Destroy(this.gameObject);

                if (x_parameter - 2 >= 0 && db.visited[x - 1][y] == false)
                {
                    db.visited[x - 1][y] = true;
                    Destroy(db.food[x - 1][y]);
                    Destroy(GameObject.Find(this.gameObject.name));
                    clear1(x - 2, y);
                }

                if (x_parameter + 2 < 10 && db.visited[x + 1][y] == false)
                {
                    db.visited[x + 1][y] = true;
                    Destroy(db.food[x + 1][y]);
                    Destroy(GameObject.Find(this.gameObject.name));
                    clear1(x + 2, y);
                }

                if (y_parameter - 1 >= 0 && db.visited[x][y - 1] == false)
                {
                    db.visited[x][y - 1] = true;
                    Destroy(db.food[x][y - 1]);
                    Destroy(GameObject.Find(this.gameObject.name));
                    clear1(x, y - 1);
                }

                if (y_parameter + 1 < 7 && db.visited[x][y + 1] == false)
                {
                    db.visited[x][y + 1] = true;
                    Destroy(db.food[x][y + 1]);
                    Destroy(GameObject.Find(this.gameObject.name));
                    clear1(x, y + 1);
                }
            }
        }
    }

    
    public void clear(int x, int y)
    {
        db.visited[x][y] = true;
        Destroy(db.food[x][y]);
        if (x - 1 >= 0 && db.food[x][y].name == db.food[x - 1][y].name && db.visited[x - 1][y] == false)
        {
            db.visited[x - 1][y] = true;
            deleted[x - 1][y] = true;
            Destroy(db.food[x - 1][y]);
            clear(x - 1, y);
        }

        if (x + 1 < 5 && db.food[x][y].name == db.food[x + 1][y].name && db.visited[x + 1][y] == false)
        {
            db.visited[x + 1][y] = true;
            deleted[x + 1][y] = true;
            Destroy(db.food[x + 1][y]);
            clear(x + 1, y);
        }

        if (y - 1 >= 0 && db.food[x][y].name == db.food[x][y - 1].name && db.visited[x][y - 1] == false)
        {
            db.visited[x][y - 1] = true;
            deleted[x][y - 1] = true;
            Destroy(db.food[x][y - 1]);
            clear(x, y - 1);
        }

        if (y + 1 < 7 && db.food[x][y].name == db.food[x][y + 1].name && db.visited[x][y + 1] == false)
        {
            db.visited[x][y + 1] = true;
            deleted[x][y + 1] = true;
            Destroy(db.food[x][y + 1]);
            clear(x, y + 1);
        }
    }
    

    public void setCells()
    {
        for (int i = 0; i < db.x; i++)
        {
            for (int j = 0; j < db.y; j++)
            {
                db.visited[i][j] = false;
            }
        }
    }
}