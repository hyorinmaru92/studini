﻿using UnityEngine;
using System.Collections;

public class test_swipe : MonoBehaviour 
{
	public float speed = 2.5f;
	private float k=0.0f, l=0.0f;
	private float lastPos=0;
	Touch touch;
	//private bool isMoved=false;
	//private float startPos=0;
	//private float endPos=0;
	
	void moveCam(Touch t)
	{
		k += Input.GetTouch (0).deltaPosition.x * speed * Time.deltaTime;
		l -= Input.GetTouch (0).deltaPosition.x * speed * Time.deltaTime;

		Camera.main.transform.position = new Vector3 (l, 0, -20);

		switch (t.phase) 

		{
		case TouchPhase.Moved:
			if (Camera.main.transform.position.x >= 20) 
			{
				Camera.main.transform.position = new Vector3 (20, 0, -20);
				lastPos=20;
				l=20;
			}
			else if (Camera.main.transform.position.x <= -20)
			{
				Camera.main.transform.position = new Vector3 (-20, 0, -20);
				lastPos=-20;
				l=-20;
			}

			break;
		case TouchPhase.Ended:
			//0->1
			if (Camera.main.transform.position.x >= 5 && lastPos==0) 
			{
				Camera.main.transform.position = new Vector3 (10, 0, -20);
				lastPos=10;
				l=10;
			}
			//0->-1
			else if (Camera.main.transform.position.x <= -5 && lastPos==0)
			{
				Camera.main.transform.position = new Vector3 (-10, 0, -20);
				lastPos=-10;
				l=-10;
			}
			//1->2
			else if (Camera.main.transform.position.x >= 15 && lastPos==10)
			{
				Camera.main.transform.position = new Vector3 (20, 0, -20);
				lastPos=20;
				l=20;
			}
			//-1->-2
			else if (Camera.main.transform.position.x <= -15 && lastPos==-10)
			{
				Camera.main.transform.position = new Vector3 (-20, 0, -20);
				lastPos=-20;
				l=-20;
			}
			//-2->-1
			else if (Camera.main.transform.position.x >= -15 && lastPos==-20)
			{
				Camera.main.transform.position = new Vector3 (-10, 0, -20);
				lastPos=-10;
				l=-10;
			}
			//2->1
			else if (Camera.main.transform.position.x <= 15 && lastPos==20)
			{
				Camera.main.transform.position = new Vector3 (10, 0, -20);
				lastPos=10;
				l=10;
			}
			//-1->0
			else if (Camera.main.transform.position.x >= -5 && lastPos==-10)
			{
				Camera.main.transform.position = new Vector3 (0, 0, -20);
				lastPos=0;
				l=0;
			}
			//1->0
			else if (Camera.main.transform.position.x <= 5 && lastPos==10)
			{
				Camera.main.transform.position = new Vector3 (0, 0, -20);
				lastPos=0;
				l=0;
			}
			else
			{
				//0->prawo->0
				if (Camera.main.transform.position.x < 5 && lastPos==0) 
				{
					Camera.main.transform.position = new Vector3 (0, 0, -20);
					lastPos=0;
					l=0;
				}
				//0->lewo->0
				else if (Camera.main.transform.position.x > -5 && lastPos==0) 
				{
					Camera.main.transform.position = new Vector3 (0, 0, -20);
					lastPos=0;
					l=0;
				}
				//1->prawo->1
				else if (Camera.main.transform.position.x < 15 && lastPos==10) 
				{
					Camera.main.transform.position = new Vector3 (10, 0, -20);
					lastPos=10;
					l=10;
				}
				//1->lewo->1
				else if (Camera.main.transform.position.x > 5 && lastPos==10) 
				{
					Camera.main.transform.position = new Vector3 (10, 0, -20);
					lastPos=10;
					l=10;
				}
				//2->lewo->2
				else if (Camera.main.transform.position.x > 15 && lastPos==20) 
				{
					Camera.main.transform.position = new Vector3 (20, 0, -20);
					lastPos=20;
					l=20;
				}
				//-1->prawo->-1
				else if (Camera.main.transform.position.x <-5 && lastPos==-10) 
				{
					Camera.main.transform.position = new Vector3 (-10, 0, -20);
					lastPos=-10;
					l=-10;
				}
				//-1->lewo->-1
				else if (Camera.main.transform.position.x >-15 && lastPos==-10) 
				{
					Camera.main.transform.position = new Vector3 (-10, 0, -20);
					lastPos=-10;
					l=-10;
				}
				//-2->prawo->-2
				else if (Camera.main.transform.position.x <-15 && lastPos==-20) 
				{
					Camera.main.transform.position = new Vector3 (-20, 0, -20);
					lastPos=-20;
					l=-20;
				}
			}
			break;
		}
	}

	
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.Quit();
		}

		if (Input.touches.Length > 0) 
		{
			for (int i=0; i< Input.touchCount; i++)
			{
				touch = Input.GetTouch(i);
				moveCam(touch);
			}
		}
	}
}