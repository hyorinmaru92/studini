﻿using UnityEngine;
using System.Collections;

public class audioInGame : MonoBehaviour {

	CharSettings csObject;
	public UnityEngine.UI.Button button;
	public UnityEngine.UI.Image ONsound; 
	public UnityEngine.UI.Image OFFsound;

	// Use this for initialization
	void Start () {
		csObject = GameObject.Find("CharSettings").GetComponent<CharSettings>();
		if (csObject.Audio) {
			button.image = ONsound;
			OFFsound.enabled=false;
			ONsound.enabled = true;
		} else {
			button.image = OFFsound;
			OFFsound.enabled=true;
			ONsound.enabled=false;
		}
						
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void SwitchAudio(){
		if (csObject.Audio) {
			button.image = OFFsound;
			csObject.Audio = false;
			OFFsound.enabled=true;
			ONsound.enabled=false;
		} else {
			button.image = ONsound;
			csObject.Audio = true;
			ONsound.enabled=true;
			OFFsound.enabled=false;
		}

	}
}
