﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.Threading;

public class pokemon_main : MonoBehaviour 
{
    public Sprite easyBoss;
    public Sprite easyBackground;
    public Sprite mediumBoss;
    public Sprite mediumBackground;
    public Sprite hardBoss;
    public Sprite hardBackground;
    public Transform boss, background;
    public Text hp1;
    public Text hp2;
    private SpriteRenderer render1, render2;
    public Slider playerhp, computerhp;
    private Color red, green;
    public CharSettings playerLevel;

    GameObject button1, button2, button3, button4;

    public bool gameOver = false;
    public bool computerLost = false;
    public bool playerLost = false;
    public int level = 0;
    public float damageModification;

    public int player_hp;   
    public int player_attack;
    public int player_defense;
    public int computer_hp;
    public int computer_attack;
    public int computer_defense;
    public int player_crit;
    public int computer_crit;
    public int tour = 0;
    public static int player_hp_current;
    public static int computer_hp_current;
	public int points;

    public bool computermove = false;

    List<string> moves;

    void Start()
    {
		damageModification = 0;//playerLevel.Level * 0.1f;
        moves = new List<string>();
        render1 = boss.GetComponent<SpriteRenderer>();
        render2 = background.GetComponent<SpriteRenderer>();
        button1 = GameObject.Find("atak");
        button2 = GameObject.Find("obrona");
        button3 = GameObject.Find("cios");
        button4 = GameObject.Find("oslabienie");
        red = Color.red;
        green = Color.green;
		computer_hp_current = 100;
		player_hp_current = 100;
        hp1.text = player_hp.ToString();
        hp2.text = computer_hp.ToString();
    }

    public void setDifficultLevel(int level)
    {
        if (level == 1)
        {
            render1.sprite = easyBoss;
            render2.sprite = easyBackground;
            computer_attack = 10;
            computer_defense = 3;
            computer_hp = 100;
            player_crit = 10;
            computer_crit = 10;
            player_hp = 100;
            player_attack = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_defense = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_hp_current = 100;
            computer_hp_current = 100;
			points=100;
        }
        else if (level == 2)
        {
            render1.sprite = mediumBoss;
            render2.sprite = mediumBackground;
            computer_attack = 7;
            computer_defense = 10;
            computer_hp = 100;
            player_crit = 12;
            computer_crit = 8;
            player_hp = 100;
            player_attack = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_defense = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_hp_current = 100;
            computer_hp_current = 100;
			points=250;
        }
        else
        {
            render1.sprite = hardBoss;
            render2.sprite = hardBackground;
            computer_attack = 12;
            computer_defense = 10;
            computer_hp = 100;
            player_crit = 14;
            computer_crit = 6;
            player_hp = 100;
            player_attack = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_defense = 5 + System.Convert.ToInt32(Mathf.Ceil(damageModification));
            player_hp_current = 100;
            computer_hp_current = 100;
			points=400;
        }
		hp1.color = green;
		hp2.color = green; 
		gameOver = false;
    }

    void Update()
    {
        hp1.text = player_hp_current.ToString();
        hp2.text = computer_hp_current.ToString();
        playerhp.value = player_hp_current;
        computerhp.value = computer_hp_current;

        if (computer_hp_current <= 30)
        {
            hp2.color = red;
        }

        if (player_hp_current <= 30)
        {
            hp1.color = red;
        }

        if (computer_hp_current <= 0)
        {
            gameOver = true;
            computerLost = true;
        }

        if (player_hp_current <= 0)
        {
            gameOver = true;
            playerLost = true;
        }

        while (computermove)
        {
            nextMove();
        }
    }

    public void nextMove()
    {
        if (level == 1)
        {
            int move = Random.Range(0, 4);
            switch (move)
            {
                case 0:
                    computerAttack();
                    break;
                case 1:
                    computerDefense();
                    break;
                case 2:
                    computerHit();
                    break;
                case 3:
                    computerWeaken();
                    break;
            } 
        }
        else if (level == 2)
        {
            if (tour == 0)
            {
                string temp = moves[moves.Count - 1];
                switch (temp)
                {
                    case "attack":
                        computerHit();
                        break;
                    case "defense":
                        computerWeaken();
                        break;
                    case "hit":
                        computerHit();
                        break;
                    case "weaken":
                        computerHit();
                        break;
                }
                tour++;
            }
            else
            {
                if (computer_hp_current > 30)
                {
                    string temp = moves[moves.Count - 1];
                    switch (temp)
                    {
                        case "attack":
                            int move1 = Random.Range(0, 2);
                            switch (move1)
                            {
                                case 0:
                                    computerAttack();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "defense":
                            int move2 = Random.Range(0, 2);
                            switch (move2)
                            {
                                case 0:
                                    computerWeaken();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "hit":
                            int move3 = Random.Range(0, 2);
                            switch (move3)
                            {
                                case 0:
                                    computerDefense();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "weaken":
                            int move4 = Random.Range(0, 2);
                            switch (move4)
                            {
                                case 0:
                                    computerAttack();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                    }
                }
                else
                {
                    int move = Random.Range(0, 4);
                    switch (move)
                    {
                        case 0:
                            computerDefense();
                            break;
                        case 1:
                            computerHit();
                            break;
                        case 2:
                            computerWeaken();
                            break;
                        case 3:
                            computerHit();
                            break;
                    }
                }  
            }  
        }
        else
        {
            if (tour == 0)
            {
                string temp = moves[moves.Count - 1];
                switch (temp)
                {
                    case "attack":
                        computerHit();
                        break;
                    case "defense":
                        computerWeaken();
                        break;
                    case "hit":
                        computerHit();
                        break;
                    case "weaken":
                        computerHit();
                        break;
                }
                tour++;
            }
            else
            {
                if (computer_hp_current > 50)
                {
                    string temp = moves[moves.Count - 1];
                    switch (temp)
                    {
                        case "attack":
                            int move1 = Random.Range(0, 2);
                            switch (move1)
                            {
                                case 0:
                                    computerAttack();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "defense":
                            int move2 = Random.Range(0, 2);
                            switch (move2)
                            {
                                case 0:
                                    computerWeaken();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "hit":
                            int move3 = Random.Range(0, 2);
                            switch (move3)
                            {
                                case 0:
                                    computerDefense();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "weaken":
                            int move4 = Random.Range(0, 2);
                            switch (move4)
                            {
                                case 0:
                                    computerAttack();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                            }
                            break;
                    }
                }
                else
                {
                    string temp = moves[moves.Count - 1];
                    switch (temp)
                    {
                        case "attack":
                            int move1 = Random.Range(0, 3);
                            switch (move1)
                            {
                                case 0:
                                    computerWeaken();
                                    break;
                                case 1:
                                    computerAttack();
                                    break;
                                case 2:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "defense":
                            int move2 = Random.Range(0, 3);
                            switch (move2)
                            {
                                case 0:
                                    computerWeaken();
                                    break;
                                case 1:
                                    computerAttack();
                                    break;
                                case 2:
                                    computerHit();
                                    break;
                            }
                            break;
                        case "hit":
                            int move3 = Random.Range(0, 3);
                            switch (move3)
                            {
                                case 0:
                                    computerWeaken();
                                    break;
                                case 1:
                                    computerHit();
                                    break;
                                case 2:
                                    computerHit();
                                    break;
                            }
                            break;
                        default:
                            computerHit();
                            break;
                    }
                }
            }
        }
    }

    public void playerAttack()
    {
        if (player_attack == 20)
        {
            player_attack = 20;
        }
        else if (player_attack >= 18)
        {
            player_attack = 20;
        }
        else
        {
            player_attack += 2;
        }
        
        moves.Add("attack");
        Debug.Log("attack" + player_attack);
        computermove = true;
    }

    public void playerDefense()
    {
        if (player_defense == 20)
        {
            player_defense = 20;
        }
        else if (player_defense >= 18)
        {
            player_defense = 20;
        }
        else
        {
            player_defense += 2;
        }
        moves.Add("defense");
        Debug.Log("defense" + player_defense);
        computermove = true;
    }

    public void playerHit()
    {
        int crit = Random.Range(0, player_crit);
        if (computer_defense > 0)
        {
            if (crit == 1)
            {
                computer_hp_current = computer_hp_current - 2 * System.Convert.ToInt32(player_attack * (1 - ((computer_defense * 4) / 100)));
            }
            else
            {
                computer_hp_current = computer_hp_current - System.Convert.ToInt32(player_attack * (1 - ((computer_defense * 4) / 100)));
            }
        }
        else
        {
            if (crit == 1)
            {
                computer_hp_current = computer_hp_current - 2 * System.Convert.ToInt32(player_attack * (1 - ((computer_defense * 4) / 100)));
            }
            else
            {
                computer_hp_current = computer_hp_current - System.Convert.ToInt32(player_attack * (1 - ((computer_defense * 4) / 100)));
            }
        }

        if (computer_hp_current <= 0)
        {
            computer_hp_current = 0;
            gameOver = true;
        }
        else
        {
            moves.Add("hit");
            Debug.Log("hit"+computer_hp_current);
            computermove = true;
        }
        
    }

    public void playerWeaken()
    {
        if (computer_defense <= 0)
        {
            computer_defense = 0;
            computer_crit = computer_crit + 2;
            computer_hp_current -= 1;
        }
        else
        {
            computer_defense = computer_defense - 3;
            computer_crit = computer_crit + 2;
            computer_hp_current -= 1;
        }
        moves.Add("weaken");
        Debug.Log("weaken"+computer_defense);
        computermove = true;
    }

    public void computerAttack()
    {
        if (computer_attack == 20)
        {
            computer_attack = 20;
        }
        else if (computer_attack >= 18)
        {
            computer_attack = 20;
        }
        else
        {
            computer_attack += 2;
        }
        computermove = false;
        Debug.Log("computer attack "+computer_attack);
    }

    public void computerDefense()
    {
        if (computer_defense == 20)
        {
            computer_defense = 20;
        }
        else if (computer_defense >= 18)
        {
            computer_defense = 20;
        }
        else
        {
            computer_defense += 2;
        }
        computermove = false;
        Debug.Log("computer defense "+computer_defense);
    }

    public void computerHit()
    {
        int crit = Random.Range(0, player_crit);
        if (player_defense > 0)
        {
            if (crit == 1)
            {
                player_hp_current = player_hp_current - 2 * System.Convert.ToInt32(computer_attack * (1 - ((player_defense * 4) / 100)));
            }
            else
            {
                player_hp_current = player_hp_current - System.Convert.ToInt32(computer_attack * (1 - ((player_defense * 4) / 100)));
            }
        }
        else
        {
            if (crit == 1)
            {
                player_hp_current = player_hp_current - 2 * System.Convert.ToInt32(computer_attack * (1 - ((player_defense * 4) / 100)));
            }
            else
            {
                player_hp_current = player_hp_current - System.Convert.ToInt32(computer_attack * (1 - ((player_defense * 4) / 100)));
            }
        }
        if (player_hp_current <= 0)
        {
            player_hp_current = 0;
        }
        computermove = false;
        Debug.Log("Computer hit: " + player_hp_current);
    }

    public void computerWeaken()
    {
        if (player_defense <= 0)
        {
            player_defense = 0;
            player_crit = player_crit + 2;
        }
        else
        {
            player_defense = player_defense - 3;
            player_crit = player_crit + 2;
        }
        computermove = false;
        Debug.Log("Computer weaken "+player_defense);
    }
}