﻿using UnityEngine;
using System.Collections;

public class SettingsPanel : MonoBehaviour {

    public Transform _panel;

    public void FadeOutShowIn()
    {
        Animator anim = _panel.GetComponent<Animator>();
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("empty"))
            anim.Play("ShowIn");
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("ShowIn"))
            anim.Play("FadeOut");
        if(anim.GetCurrentAnimatorStateInfo(0).IsName("FadeOut"))
            anim.Play("ShowIn");
    }
}
