﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PartyPanel : MonoBehaviour {
	public GameObject partyCanvas;
	public GameObject[] alkohol;
	public GameObject buttonsCanvas;
	public Text userMoney;
	public Text alkoholCost;

	GameObject instance;
	CharSettings csObject;
	int currentIndex;
	int alkoUp = 15;
	int alkoCost = 3;

	public void displayPartyCanvas() {
		partyCanvas.SetActive (true);
		buttonsCanvas.SetActive (false);
		csObject = GameObject.Find ("CharSettings").GetComponent<CharSettings> ();
		userMoney.text = "Your money: " + csObject.Money.ToString();
		alkoholCost.text = "Cost: " + alkoCost + "$";

		if (alkohol.Length > 0) {
			currentIndex = 0;
			instance = Instantiate (alkohol[currentIndex], new Vector3(0.19f, -0.4f, -1.5f), Quaternion.identity) as GameObject;
		}
	}

	public void closePartyCanvas() {
		Destroy (instance);
		buttonsCanvas.SetActive (true);
		partyCanvas.SetActive (false);
	}

	public void drinkAlkohol() {
		if (csObject.Money >= alkoCost) {
			csObject.Money -= alkoCost;
			csObject.Need [(int)Needs.alko] += alkoUp;
			userMoney.text = "Your money: " + csObject.Money.ToString()+"$";
		}
	}

	public void nextAlkohol() {
		Destroy (instance);
		currentIndex = (++currentIndex) % alkohol.Length;

		instance = Instantiate (alkohol[currentIndex], new Vector3(0.19f, -0.4f, -1.5f), Quaternion.identity) as GameObject;
	}

	public void previousAlkohol() {
		Destroy (instance);

		if (currentIndex > 0) {
			currentIndex = --currentIndex;
		} else {
			currentIndex = alkohol.Length - 1;
		}

		instance = Instantiate (alkohol[currentIndex], new Vector3(0.19f, -0.4f, -1.5f), Quaternion.identity) as GameObject;
	}

	// Use this for initialization
	void Start () {
		csObject = GameObject.Find ("CharSettings").GetComponent<CharSettings> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
