﻿using UnityEngine;
using System.Collections;

public class PartyButtons : MonoBehaviour {
	public GameObject partyPanel;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void displayPartyPanel() {
		partyPanel.SetActive (true);
		FindObjectOfType<PartyPanel> ().displayPartyCanvas ();
	}

	public void closePartyPanel() {
		FindObjectOfType<PartyPanel> ().closePartyCanvas ();
		partyPanel.SetActive (false);
	}

	public void nextAlkohol() {
		FindObjectOfType<PartyPanel> ().nextAlkohol ();
		Debug.Log ("nextAlkohol");
	}

	public void previousAlkohol() {
		FindObjectOfType<PartyPanel> ().previousAlkohol ();
		Debug.Log ("previousAlkohol");
	}

	public void drinkAlkohol() {
		FindObjectOfType<PartyPanel> ().drinkAlkohol ();
		Debug.Log ("drinkAlkohol");
	}
}
