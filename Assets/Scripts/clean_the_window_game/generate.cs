﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class generate : MonoBehaviour 
{

	private int difficulty = 10;
	private bool gameStart=false;
	private int level=0;

	void Start () 
	{
	}

	void Update ()
	{
		if (gameStart) {
			gameStart=false;
			drop ();
		}

		if (GameObject.Find ("dirt1(Clone)") || GameObject.Find ("dirt2(Clone)") || GameObject.Find ("dirt3(Clone)") || GameObject.Find ("dirt4(Clone)")) {
			//Debug.Log ("tak");
		} else {
			reset ();
		}
					

	}

	public void setDifficultLevel(int lev)
	{
		if (lev == 1)
			difficulty = 10;
		else if (lev == 2)
			difficulty = 15;
		else
			difficulty = 20;
		gameStart = true;
		FindObjectOfType<timer> ().timerStart();
	}

	public void drop()
	{
		for (int i=0; i<= difficulty; i++) 
		{
			int move = Random.Range (0, 4);
			switch (move) 
			{
			case 0:
				Instantiate (Resources.Load ("dirt1"), new Vector3(Random.Range(-4,4), Random.Range(-7, 7), 0), transform.rotation);
				break;
			case 1:
				Instantiate (Resources.Load ("dirt2"), new Vector3(Random.Range(-4,4), Random.Range(-7, 7), 0), transform.rotation);
				break;
			case 2:
				Instantiate (Resources.Load ("dirt3"), new Vector3(Random.Range(-4,4), Random.Range(-7, 7), 0), transform.rotation);
				break;
			case 3:
				Instantiate (Resources.Load ("dirt4"), new Vector3(Random.Range(-4,4), Random.Range(-7, 7), 0), transform.rotation);
				break;
			default:
				break;
			}
		}
		level++;
	}

	public void reset()
	{
		if (level > 0) {
			FindObjectOfType<timer> ().setTimer (1);
			drop ();
		}
	}

	public int getPoints()
	{
		return 25 * level;
	}
}
