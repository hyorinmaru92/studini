﻿using UnityEngine;
using System.Collections;

public class cleaning : MonoBehaviour 
{	
	Touch touch;
	private SpriteRenderer render;
	private Color objColor;
	public Collider2D col;

	void Start()
	{

	}

	void Update()
	{

		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("work_scene");
		}

		if (Input.touchCount > 0)
		{
			Vector3 wp = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
			Vector2 touchPos = new Vector2(wp.x, wp.y);
			if (collider2D == Physics2D.OverlapPoint(touchPos))
			{
				for (int i=0; i< Input.touchCount; i++)
				{
					touch = Input.GetTouch(i);
					clean (touch);
				}
				
			}
		}
	}

	public void clean(Touch touch)
	{
		if(this.gameObject.tag == "Dirt")
		{
			render = gameObject.GetComponent <SpriteRenderer> ();
			objColor = render.color;
			RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(touch.position), Vector2.zero);
			Vector3 pos = Camera.main.ScreenToWorldPoint(touch.position);
			Color fade = new Color (255, 255, 255, 0.2f);
			if(render.color.a < 0.25)
			{
				if(hit.collider != null)
				{
					if(hit.collider.gameObject==gameObject) Destroy(gameObject);
				}
			}
			
			if (touch.phase == TouchPhase.Moved) 
			{
				render.color = Color.Lerp (render.color, fade, 0.05f*Time.time);
				if(render.color.a < 0.25)
				{
					if(hit.collider != null)
					{
						if(hit.collider.gameObject==gameObject) Destroy(gameObject);
					}
				}
			}
		}
	}
}
