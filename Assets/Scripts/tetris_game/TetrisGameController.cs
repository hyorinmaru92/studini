﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TetrisGameController : MonoBehaviour {
	public static int score = 0;
	public static int successPoints = 10; // Liczba punktów, które otrzyma gracz po zapełnieniu wiersza
	public Text textScore;
	
	public void addScore() {
		score += successPoints;
		textScore.text = score.ToString ();
	}

	// Use this for initialization
	void Start () {
		textScore.text = score.ToString();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("work_scene");
		}
	}
}
