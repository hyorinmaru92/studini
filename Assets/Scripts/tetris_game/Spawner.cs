﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
	// Tablica przechowująca obiekty (grupy)
	public GameObject[] shapes;
	public double difficultTime=0;
	bool started = false;

	public void generateShape() {
		// Wczytanie wielkości tablicy i wylosowanie losowej liczby z zadanego zakresu
		int length = shapes.Length;
		int i = Random.Range (0, length);

		// Wygenerowanie obiektu w bieżącej pozycji
		// Quaternion.indentity - domyślna rotacja
		Instantiate (shapes [i], transform.position, Quaternion.identity); 
	}



	// Use this for initialization
	void Start () {


	}
	
	// Update is called once per frame
	void Update () {
		// Generowanie nowego kształtu, po włączeniu gry
		if (difficultTime != 0 && !started) {
			generateShape ();
			started = true;
		}
	}

	public void setDifficultLevel(int level)
	{
		if (level == 1)
			difficultTime = 1;
		else if (level == 2)
			difficultTime = 0.85;
		else
			difficultTime = 0.75;
	}

}
