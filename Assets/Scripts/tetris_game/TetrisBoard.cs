﻿using UnityEngine;
using System.Collections;

public class TetrisBoard : MonoBehaviour {
	public static int boardWidth = 10;
	public static int boardHeight = 20;
	public static double firstIndex = 0;
	public static double lastIndex = 4.5;
	public static Transform[,] board = new Transform[boardWidth, boardHeight];
	public static Vector3 vecDown = new Vector3 (0, (float)-0.5, 0);


	// Funkcja korygująca możliwości błędów zaokrąglenia podczas przenoszenia elementów
	public static Vector2 roundToHalf(Vector2 vec) {
		double x = (System.Math.Round (2 * vec.x))/2;
		double y = (System.Math.Round (2 * vec.y))/2;
		
		Vector2 newVector = new Vector2 ((float)x, (float)y);

		return newVector;
	}

	// Funkcja sprawdzająca, czy obiekt znajduje się wewnątrz tablicy
	public static bool isInABoard(Vector2 vec) {
		Debug.Log ("isInABoard: " + vec);
		if ((double)vec.x >= 0 && (double)vec.y >= 0 && (double)vec.x <= lastIndex)
			return true;
	
		return false;
	}

	// Funkcja usuwająca konkretny wiersz	
	public static void destroyRow(int row) {
		for (int x = 0; x < boardWidth; ++x) {
			Destroy( board[x, row].gameObject );
			board[x, row] = null;
		}
	}

	public static void decreaseAllRows(int row) {
		// Pętla po wszystkich wierszach od zadanego do najwyższego 
		for (int y = row; y < boardHeight; ++y) {
			for (int x = 0; x < boardWidth; ++x) {
				// Czy w danym miejscu znajduje się klocek
				if ( board[x, y] != null ) {
					// Obniżenie elementu o jeden
					board[x, y-1] = board[x, y];
					board[x, y] = null;
					// Zmiana pozycji na tablicy gry
					board[x, y-1].position += vecDown;
				}
			}
		}
	}

	public static bool isRowFilled(int row) {
		for (int x = 0; x < boardWidth; ++x) {
			if ( board[x, row] == null ) 
				return false;
		}
		return true;
	}

	public static void deleteAllFilledRows() {
		for (int row = 0; row < boardHeight; ++row) {
			if( isRowFilled (row) ) {
				destroyRow(row);
				FindObjectOfType<TetrisGameController> ().addScore ();
				decreaseAllRows(row+1);
				--row;
			}
		}
	}



	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
