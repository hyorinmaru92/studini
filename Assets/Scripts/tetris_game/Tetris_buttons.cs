﻿using UnityEngine;
using System.Collections;

public class Tetris_buttons : MonoBehaviour {

	public void moveDownButton() {
		FindObjectOfType<Shapes> ().moveDown ();
	}

	public void moveLeftButton() {
		FindObjectOfType<Shapes> ().moveLeft ();
	}

	public void moveRightButton() {
		FindObjectOfType<Shapes> ().moveRight ();
	}

	public void moveRotateButton() {
		FindObjectOfType<Shapes> ().moveRotate ();
	}


	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	}
}
