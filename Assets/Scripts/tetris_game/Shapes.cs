﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Shapes : MonoBehaviour {
	float time = 0;
	public static Vector3 vecDown = new Vector3 (0, (float)-0.5, 0);
	public static Vector3 vecLeft = new Vector3 ((float)-0.5, 0, 0);
	public static Vector3 vecRight = new Vector3 ((float)0.5, 0, 0);
	int endGame=0;

	public void moveRotate() {
		transform.Rotate(0, 0, -90);
		if (isValidPosition())
			boardUpdate();
		else
			transform.Rotate(0, 0, 90);
	}


	
	public void moveRight() {
		// Modify position
		transform.position += vecRight;
		// See if valid
		if (isValidPosition())
			// It's valid. Update grid.
			boardUpdate();
		else
			// It's not valid. revert.
			transform.position += vecLeft;
	}
	
	public void moveLeft() {
		// Modify position
		transform.position += vecLeft;
		// See if valid
		if (isValidPosition())
			// It's valid. Update grid.
			boardUpdate();
		else
			// It's not valid. revert.
			transform.position += vecRight;
	}
	
	public void moveDown() {
		transform.position += vecDown;
		
		if (isValidPosition ()) {
			boardUpdate ();
		} else {
			transform.position += new Vector3 (0, (float)0.5, 0);
			
			TetrisBoard.deleteAllFilledRows ();
			
			enabled = false;
			
			FindObjectOfType<Spawner> ().generateShape ();
		}
		time = Time.time;
	}

	// Funkcja walidująca pozycję (jeżeli obiekt znajduje się poza obszarem gry, 
	// nachodzi na inny obiekt zwraca fałsz	
	bool isValidPosition() {
		foreach (Transform blocks in transform) {
			// Zaokrąglenie możliwych błędów przybliżenia
			Vector2 vec = TetrisBoard.roundToHalf(blocks.position);
			int x = (int)(vec.x*2);
			int y = (int)(vec.y*2);
		
			// Sprawdzenie, czy obiekt znajduje się wewnątrz obszaru gry
			if ( !TetrisBoard.isInABoard(vec) )
				return false;

			// Sprawdzenie, czy obiekt nie nachodzi na inny
			if ( TetrisBoard.board[x, y] != null && TetrisBoard.board[x, y].parent != transform )
				return false;
		}
		return true;
	}

	void boardUpdate() {
		for (int y = 0; y < TetrisBoard.boardHeight; ++y) {
			for (int x = 0; x < TetrisBoard.boardWidth; ++x) {
				if (TetrisBoard.board[x, y] != null) 
					if (TetrisBoard.board[x, y].parent == transform)
						TetrisBoard.board[x, y] = null;
			}
		}

		foreach (Transform blocks in transform) {
			Vector2 vec = TetrisBoard.roundToHalf(blocks.position);
			TetrisBoard.board[(int)(vec.x*2), (int)(vec.y*2)] = blocks;
		}
	}


	// Use this for initialization
	void Start () {
		if (!isValidPosition ()) {
			endGame=1;
			//Destroy(gameObject);
		}
	}

	// Update is called once per frame
	void Update () {
		if (Time.time - time >= FindObjectOfType<Spawner> ().difficultTime) {
			moveDown ();
		}
	}

	public int ifEndGame()
	{
		return endGame;
	}
}
