﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class ProductsPanel : MonoBehaviour {
	public GameObject[] kitchenProducts;
	public GameObject productInstance;
	public GameObject canvas;
	public Text productText;
	public Text costText;
	public Text userMoney;

	int currentProductIndex;
	string productName;
	int arrayLength;
	GameObject go;
	CharSettings csObject;
	int productCost = 3;


	void Start () {

	}

	public void displayCanvas() {
		go = GameObject.Find ("CharSettings");
		csObject = go.GetComponent<CharSettings>();

		canvas.SetActive(true);
		currentProductIndex = 0;
		productInstance = Instantiate (kitchenProducts [currentProductIndex], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
		productName = productInstance.transform.GetChild (0).name;
		productText.text = productName;
		userMoney.text = "Your money: " + csObject.Money.ToString();
		costText.text = "Cost: " + productCost.ToString() + "$";
	}

	public void previousProduct() {
		if (currentProductIndex == 0)
			currentProductIndex = kitchenProducts.Length - 1;
		else
			currentProductIndex = (--currentProductIndex) % kitchenProducts.Length;

		Destroy (productInstance);
		productInstance = Instantiate (kitchenProducts [currentProductIndex], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
		productName = productInstance.transform.GetChild (0).name;
		productText.text = productName;
	}

	public void nextProduct() {
		currentProductIndex = (++currentProductIndex) % kitchenProducts.Length;

		Destroy (productInstance);
		productInstance = Instantiate (kitchenProducts [currentProductIndex], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
		productName = productInstance.transform.GetChild (0).name;
		productText.text = productName;
	}

	public void closeWindow() {
		Destroy (productInstance);
		currentProductIndex = 0;
		productText.text = "";
		canvas.SetActive (false);
		FindObjectOfType<kitchenController> ().closePanel ();
	}

	public void buyCurrentProduct() {
		string[] products = Enum.GetNames (typeof(Products));
		int enumIndex = -1;

		if (csObject.Money >= productCost) {
			csObject.Money -= productCost;
			userMoney.text = "Your money: " + csObject.Money.ToString();
			for (int i = 0; i < products.Length; ++i) {
				if (products [i].ToString () == productName)
						enumIndex = i;
			}

			csObject.Product [enumIndex] ++;
		}
	}


	// Update is called once per frame
	void Update () {
	
	}
}
