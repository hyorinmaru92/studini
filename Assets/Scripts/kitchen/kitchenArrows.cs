﻿using UnityEngine;
using System.Collections;

public class kitchenArrows : MonoBehaviour {
	public void next() {
		FindObjectOfType<ProductsPanel> ().nextProduct ();
	}

	public void previous() {
		FindObjectOfType<ProductsPanel> ().previousProduct ();
	}

	public void buyProduct() {
		FindObjectOfType<ProductsPanel> ().buyCurrentProduct ();
	}
	
	public void exit() {
		FindObjectOfType<ProductsPanel> ().closeWindow ();
	}

	public void fridgeNext() {
		FindObjectOfType<Fridge> ().nextFridgeProduct ();
	}

	public void fridgePrevious() {
		FindObjectOfType<Fridge> ().previousFridgeProduct ();
	}

	public void fridgeEat() {
		FindObjectOfType<Fridge> ().eatProduct ();
	}

	public void fridgeClose() {
		FindObjectOfType<Fridge> ().fridgeClose();
	}

	public void gamesClose() {
		FindObjectOfType<KitchenGames> ().closeGamesCanvas ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
