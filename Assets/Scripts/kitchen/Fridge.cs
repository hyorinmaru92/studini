using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;


public class Fridge : MonoBehaviour {
	public GameObject instance;
	public Text productTitle;
	public GameObject fridgeCanvas;
	public GameObject[] prefabs;				// Tablica prefabów

	GameObject productInstance;					// Instancja istniejącego obiektu (prefab)
	int currentProductIndex;					// Indeks bieżącego produktu
	string productName;							// Nazwa bieżącego produktu i jego ilość
	int arrayLength, productCount;
	GameObject go;
	CharSettings csObject;						// Obiekt klasy CharSettings
	Dictionary<string, int> productsInFridge;
	string[] names;
	int[] counts;


	// Use this for initialization
	void Start () {
		fridgeCanvas.SetActive (true);
	}

	public void fridgeDisplay() {
		fridgeCanvas.SetActive (true);
	}

	public void fridgeClose() {
		Destroy(productInstance);
		fridgeCanvas.SetActive (false);
		FindObjectOfType<kitchenController> ().closeFridgePanel ();
	}

	public void displayFridgeCanvas() {
		go = GameObject.Find ("CharSettings");
		csObject = go.GetComponent<CharSettings>();

		fridgeCanvas.SetActive (true);
	
		productsInFridge = loadBoughtProducts ();
		if (productsInFridge.Count == 0) {
			productTitle.text = "Fridge is empty";
		} else {
			currentProductIndex = 0;
			reloadTables ();
		}
	}

	public void reloadTables() {
		names = new string[productsInFridge.Count];
		counts = new int[productsInFridge.Count];
		int i = 0;
		
		foreach (KeyValuePair<string, int> productAndCost in productsInFridge) {
			names[i] = productAndCost.Key;
			counts[i] = productAndCost.Value;
			i++;
		}
		
		productName = names [currentProductIndex];
		productCount = counts [currentProductIndex];
		productTitle.text = productName + " " + productCount.ToString() + "x";
		
		for (int j = 0; j < prefabs.Length; j++) {
			if (prefabs[j].transform.GetChild (0).name == productName) {
				productInstance = Instantiate (prefabs[j], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
			}
		}
	}

	public Dictionary<string, int> loadBoughtProducts() {
		string[] products = Enum.GetNames (typeof(Products));
		var boughtProducts = new Dictionary<string, int> ();
		
		for (int i=0; i < products.Length; ++i) {
			if (csObject.Product[i] > 0)
			{
				boughtProducts.Add( products[i], csObject.Product[i]);
			}
		}
		return boughtProducts;
	}

	public void eatProduct() {
		string[] products = Enum.GetNames (typeof(Products));
		int enumIndex = -1;
		
		for (int i = 0; i < products.Length; ++i) {
			if (products [i].ToString () == productName)
				enumIndex = i;
		}
		
		csObject.Product [enumIndex] --;
		csObject.Need[(int)Needs.hunger] += 15;
		if (csObject.Need [(int)Needs.hunger] > 100)
			csObject.Need [(int)Needs.hunger] = 100;


		if (productCount > 1) {
			productCount--;
			productTitle.text = productName + " " + productCount + "x";
		} else {
			Destroy(productInstance);
			productInstance = null;
			productsInFridge = loadBoughtProducts();
			if (productsInFridge.Count == 0)
			{
				productTitle.text = "Fridge is empty";
			} else {
				if (currentProductIndex > 0) 
					--currentProductIndex; 
				reloadTables();
			}
		}
	}

	public void nextFridgeProduct() {
		currentProductIndex = (++currentProductIndex) % productsInFridge.Count;

		Destroy(productInstance);
		productName = names [currentProductIndex];
		productCount = counts [currentProductIndex];
		productTitle.text = productName + " " + productCount.ToString() + "x";
		
		for (int j = 0; j < prefabs.Length; j++) {
			if (prefabs[j].transform.GetChild (0).name == productName) {
				productInstance = Instantiate (prefabs[j], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
			}
		}
	}

	public void previousFridgeProduct() {
		if (currentProductIndex == 0) 
			currentProductIndex = productsInFridge.Count - 1;
		else 
			currentProductIndex = (--currentProductIndex) % productsInFridge.Count;

		Destroy(productInstance);
		productName = names [currentProductIndex];
		productCount = counts [currentProductIndex];
		productTitle.text = productName + " " + productCount.ToString() + "x";
		
		for (int j = 0; j < prefabs.Length; j++) {
			if (prefabs[j].transform.GetChild (0).name == productName) {
				productInstance = Instantiate (prefabs[j], new Vector3(1.5f, -4f, -1f), Quaternion.identity) as GameObject;
			}
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
