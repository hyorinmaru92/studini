﻿using UnityEngine;
using System.Collections;

public class KitchenGames : MonoBehaviour {
	public GameObject gamesCanvas;

	public void displayGamesCanvas () {
		gamesCanvas.SetActive (true);
	}

	public void closeGamesCanvas() {
		gamesCanvas.SetActive (false);
		FindObjectOfType<kitchen_games_button> ().closeGamesPanel ();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
