﻿using UnityEngine;
using System.Collections;

public class kitchen_games_button : MonoBehaviour {
	public GameObject Panel;

	
	public void displayGamesPanel() {
		Panel.SetActive (true);
		FindObjectOfType<KitchenGames> ().displayGamesCanvas ();
	}
	
	public void closeGamesPanel() {
		Panel.SetActive (false);
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
