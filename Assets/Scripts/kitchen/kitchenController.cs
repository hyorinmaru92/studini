﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class kitchenController : MonoBehaviour {
	public GameObject Panel;
	public GameObject FridgePanel;

	public void closePanel() {
		Panel.SetActive (false);
	}

	public void displayPanel() {
		Panel.SetActive (true);
		FindObjectOfType<ProductsPanel> ().displayCanvas ();
	}

	public void displayFridgePanel() {
		FridgePanel.SetActive (true);
		FindObjectOfType<Fridge> ().displayFridgeCanvas ();
	}

	public void closeFridgePanel() {
		FridgePanel.SetActive (false);
	}

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
