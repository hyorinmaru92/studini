﻿using UnityEngine;
using System.Collections;

public class load_fry_pan_game : MonoBehaviour {

    FryingPanGameController controller;

    public void setDifficulty(int difficult)
    {
        controller = GameObject.Find("GameController").GetComponent<FryingPanGameController>();
        switch (difficult)
        {
            case 1: controller.timeToLeft = 60; break;
            case 2: controller.timeToLeft = 40; break;
            case 3: controller.timeToLeft = 30; break;
            default:
                break;
        }
        controller.SetTime();
    }

    public void loadFryPanGame()
    {
        Application.LoadLevel("frying_pan_game");
    }
}
