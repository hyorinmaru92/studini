﻿using UnityEngine;
using System.Collections;

public class open_app : MonoBehaviour 
{
	public void loadMyLevel()
	{
		int posx = (int)Camera.main.camera.transform.position.x;
		switch (posx) 
		{
		case 10:
			Application.LoadLevel("university_scene");
			break;
		case 20:
			Application.LoadLevel("work_scene");
			break;
		case -10:
			Application.LoadLevel("party_scene");
			break;
		case -20:
			Application.LoadLevel("kitchen_scene");
			break;
		}
	}
}
