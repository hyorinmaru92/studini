﻿using UnityEngine;
using System.Collections;

public class Note_controller : MonoBehaviour {

	void Start () {
        int animationIndex = Random.Range(0, 2);
        if(animationIndex == 0)
            GetComponent<Animator>().Play("Fly1");
        else
            GetComponent<Animator>().Play("Fly2");
	}
	
}
