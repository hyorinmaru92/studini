﻿using UnityEngine;
using System.Collections;

public class traps_controller : MonoBehaviour {

    public float repeatTime = 5f;
    public Transform _bomb;
    public Transform _drug;
    public Transform _glue;

    private Transform spawnPoint;
    private Animator animator;
    private Transform window;

    public void SetDropPoint()
    {
        InvokeRepeating("SpawnTrap", 2, repeatTime);
    }
	
    public void SpawnTrap()
    {
        int windowIndex = Random.Range(0,transform.childCount);
        window = transform.GetChild(windowIndex).GetChild(0);
        spawnPoint = window.GetChild(0);
        animator = window.GetComponent<Animator>();

        animator.Play("open", 0);
        Invoke("InstantiateDropItem", animator.GetCurrentAnimationClipState(0).Length);
    }

    public Transform ChooseRandomDropItem()
    {
        int itemIndex = Random.Range(0, 10);

        if (itemIndex < 6)
            return _bomb;
        else if (itemIndex < 8 && itemIndex >= 6)
            return _glue;
        else
            return _drug;
    }

    public void InstantiateDropItem()
    {
        Transform _dropItem = ChooseRandomDropItem();
        Instantiate(_dropItem, new Vector3(spawnPoint.transform.position.x, spawnPoint.transform.position.y, spawnPoint.transform.position.z), Quaternion.identity);
        animator.Play("close", 0);
    }
}
