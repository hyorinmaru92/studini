﻿using UnityEngine;
using System.Collections;

public class Drop_Controller : MonoBehaviour {

    public float _repeatTime = 0.5f;
    public Transform _notify;
    public Camera _camera;

    public void SetDropPoint()
    {
        InvokeRepeating("SpawnNotify", 2, _repeatTime);
    }

    public void SpawnNotify()
    {
        int X = Random.Range(0 + Screen.width / 7, Screen.width - Screen.width / 7);
        Vector3 notifyPosition = Camera.main.ScreenToWorldPoint( new Vector3(X,0,0));
        Instantiate(_notify,new Vector3(notifyPosition.x,transform.position.y, transform.position.z),Quaternion.identity);
    }
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("university_scene");
		}

	}
}
