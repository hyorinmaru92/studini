﻿using UnityEngine;
using System.Collections;

public class hitScript : MonoBehaviour {

    private Animator anim;
	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.touchCount > 0)
            anim.SetBool("hit", true);
        else
            anim.SetBool("hit", false);
	}
}
