﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Assets.Scripts.labirynth;

public class Cell
{
    public Cell(int _x,int _y)
    {
        x = _x;
        y = _y;
        visited = false;
    }
    public int x { get; set; }
    public int y { get; set; }
    public bool visited { get; set; }
}

public class Maze
{
    public Maze(int _w, int _h)
    {
        w = _w;
        h = _h;
        initialize();
        dfs();
    }

    private void dfs()
    {
        search(cells[0][0]);
        while (stack.Count !=0 )
        {
            search(stack.Pop());
        }
    }

    private void search(Cell cell)
    {
        cell.visited = true;

        List<Cell> unvisited = new List<Cell>();
        unvisited = checkUnvisited(cell);
        if (unvisited.Count > 0)
        {
            stack.Push(cell);
            Cell destinationCell = unvisited[Random.Range(0, unvisited.Count)];
            carve(cell, destinationCell);
            search(destinationCell);
        }
        else return;
    }

    private void carve(Cell begin, Cell end)
    {
        int bx = begin.x * 2 + 1;
        int by = begin.y * 2 + 1;
        int ex = end.x * 2 + 1;
        int ey = end.y * 2 + 1;
        if (bx < ex)
            buildings[bx + 1][ey] = false;
        if (bx > ex)
            buildings[bx - 1][ey] = false;
        if (by < ey)
            buildings[bx][by + 1] = false;
        if (by > ey)
            buildings[bx][by - 1] = false;
    }

    private List<Cell> checkUnvisited(Cell cell)
    {
        List<Cell> unvisited = new List<Cell>();
        if (cell.x > 0 && !cells[cell.x - 1][cell.y].visited)
            unvisited.Add(cells[cell.x - 1][cell.y]);
        if (cell.y < h-1 && !cells[cell.x][cell.y+1].visited)
            unvisited.Add(cells[cell.x][cell.y + 1]);
        if (cell.y > 0 && !cells[cell.x][cell.y - 1].visited)
            unvisited.Add(cells[cell.x][cell.y - 1]);
        if (cell.x < w - 1 && !cells[cell.x + 1][cell.y].visited)
            unvisited.Add(cells[cell.x + 1][cell.y]);

        return unvisited;
    }

    private void initialize()
    {
        buildings = new List<List<bool>>();

        for (int x = 0; x < w*2+1; x++)
        {
            List<bool> temp = new List<bool>();
            for (int y = 0; y < h*2+1; y++)
            {
                if (y % 2 == 0 || x % 2 == 0)
                    temp.Add(true);
                else
                    temp.Add(false);
            }
            buildings.Add(temp);
        }

        cells = new List<List<Cell>>();
        for (int x = 0; x < w; x++)
        {
            List<Cell> temp = new List<Cell>();
            for (int y = 0; y < h; y++)
                temp.Add(new Cell(x, y));
            cells.Add(temp);
        }
        stack = new Stack<Cell>();
    }


    public int w { get; set; }
    public int h { get; set; }

    public List<List<bool>> buildings { get; set; }
    public List<List<Cell>> cells { get; set; }
    public Stack<Cell> stack { get; set; }
}

public class Labirynth_Game_Controller : MonoBehaviour {
    public int score;
    public bool isEnd = false;
    public int gameResult = 0;
    public Transform home;
    public Transform buildingBlue;
    public Transform buildingRed;
    public Transform buildingDarkBrown;
    public Transform buildingBrown;
    public Transform player;
    public Transform startingPoint;
    public Transform building;
    public Transform road;
    public Size size;
    
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("0");
		}

	}


    void Awake()
    {
        List<Transform> buildings = new List<Transform>();

        buildings.Add(buildingBlue);
        buildings.Add(buildingBrown);
        buildings.Add(buildingRed);
        buildings.Add(buildingDarkBrown);

        Maze maze = new Maze(size.width, size.height);
        for (int x = 0; x < maze.buildings.Count; x++)
        {
            for (int y = 0; y < maze.buildings[x].Count; y++)
            {
                if (x == maze.buildings.Count - 1 && y == maze.buildings[0].Count - 2)
                {
                    Instantiate(home, new Vector3(startingPoint.position.x + x * building.transform.localScale.x, startingPoint.position.y, startingPoint.position.z + y * building.transform.localScale.z), Quaternion.Euler(new Vector3(-90, 0, 0)));
                }
                if (maze.buildings[x][y])
                    Instantiate(buildings[Random.Range(0, buildings.Count)], new Vector3(startingPoint.position.x + x * building.transform.localScale.x, startingPoint.position.y, startingPoint.position.z + y * building.transform.localScale.z), Quaternion.Euler(new Vector3(-90, 0, 0)));
                else
                    Instantiate(road, new Vector3(startingPoint.position.x + x * building.transform.localScale.x, startingPoint.position.y - 4f, startingPoint.position.z + y * building.transform.localScale.z), Quaternion.Euler(new Vector3(90, 0, 0)));
            }
        }
        int xPos = Random.Range(0, maze.buildings.Count - 1);
        int yPos = Random.Range(0,maze.buildings[0].Count-1);
        while (maze.buildings[xPos][yPos])
        {
            xPos = Random.Range(0, maze.buildings.Count - 1);
            yPos = Random.Range(0, maze.buildings[0].Count - 1);
        }
        if(maze.buildings[1][2])
            Instantiate(player, new Vector3(startingPoint.position.x + building.transform.localScale.x, startingPoint.position.y - 3.5f, startingPoint.position.z + building.transform.localScale.z), Quaternion.Euler(new Vector3(0, 90, 0)));
        else
            Instantiate(player, new Vector3(startingPoint.position.x + building.transform.localScale.x, startingPoint.position.y - 3.5f, startingPoint.position.z + building.transform.localScale.z), Quaternion.Euler(new Vector3(0, 0, 0)));
    }

}
