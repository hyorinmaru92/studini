﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class labirynth_time : MonoBehaviour {
    Text _time;
    public float _timeToGameOver;
    bool isSet = false;
    float timeFromStart = 0;
    Labirynth_Game_Controller controller;
    public float leftTime;

    public void setDifficulty(int difficult)
    {
        controller = GameObject.Find("GameController").GetComponent<Labirynth_Game_Controller>();
        switch (difficult)
        {
            case 1: _timeToGameOver = 300; break;
            case 2: _timeToGameOver = 200; break;
            case 3: _timeToGameOver = 120; break;
            default:
                break;
        }
        SetTime();
    }

    public void SetTime()
    {
        timeFromStart = Time.time;
        isSet = true;
    }


	// Use this for initialization
	void Start () 
    {
        _time = GetComponent<Text>();
	}
	
	// Update is called once per frame
	void Update () 
    {
        if (isSet)
        {
            leftTime = _timeToGameOver + timeFromStart - Time.time;
            if (_timeToGameOver + timeFromStart - Time.time < 0)
            {
                setEndGame();
            }
            int min = (int)Mathf.Floor((_timeToGameOver + timeFromStart - Time.time) / 60);
            int sec = (int)Mathf.Floor((_timeToGameOver + timeFromStart - Time.time) % 60);
            _time.text = min.ToString("00") + ":" + sec.ToString("00");
        }
	}

    private void setEndGame()
    {
        controller.isEnd = true;
    }
}
