﻿using UnityEngine;
using System.Collections;

public class labirynth_player_controller : MonoBehaviour {
    public float speed = 0.9f;
    public AudioSource walkSound;
    Direction direction = Direction.none;
    bool isMoving = false;
    Vector3 oldPosition;
    Vector3 destPosition;
    GameObject player;
    labirynth_player_controller playerScript;
    Labirynth_Game_Controller controller;
    CharSettings settings;


    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Home")
        {
            labirynth_time time = GameObject.Find("time").GetComponent<labirynth_time>();
            controller.score = (int)(((time.leftTime)/ time._timeToGameOver)*500);
            controller.isEnd = true;
        }
    }

    public enum Direction
    {
        up=0,down,left,right,none
    }

	void Start () {
        controller = GameObject.Find("GameController").GetComponent<Labirynth_Game_Controller>();
        walkSound = GameObject.Find("audioWalk").GetComponent<AudioSource>();
        player = GameObject.Find("player(Clone)");
        playerScript = player.GetComponent<labirynth_player_controller>();
        settings = GameObject.Find("CharSettings").GetComponent<CharSettings>();
        if (settings.Audio)
            GetComponent<AudioSource>().Play();
	}
	
    public void movePlayer(int directionButton)
    {
        if (!isMoving)
        {
            player = GameObject.Find("player(Clone)");
            playerScript = player.GetComponent<labirynth_player_controller>();
            playerScript.oldPosition = player.rigidbody.transform.position;
            Debug.Log(player.rigidbody.transform.position.ToString());
            switch (directionButton)
            {
                case 0: playerScript.direction = Direction.up; playerScript.destPosition = player.rigidbody.position + Vector3.forward * 1; break;
                case 1: playerScript.direction = Direction.down; break;
                case 2: playerScript.direction = Direction.left; break;
                case 3: playerScript.direction = Direction.right; break;
                default:
                    break;
            }
        }
    }

	void FixedUpdate () {
        if (Input.touchCount > 0 && Input.GetTouch(0).phase == TouchPhase.Moved)
        {
            rigidbody.angularVelocity = new Vector3(0, 0, 0);
            rigidbody.velocity = new Vector3(0, 0, 0);
            Vector2 touchDeltaPosition = Input.GetTouch(0).deltaPosition;
            if(touchDeltaPosition.y>0)
                transform.Translate(Vector3.forward * touchDeltaPosition.y * speed * Time.deltaTime);
            transform.Rotate(new Vector3(0, 1, 0) * touchDeltaPosition.x * 7f * Time.deltaTime);
            if(!walkSound.isPlaying && settings.Audio)
                walkSound.audio.Play();
        }
        if (Input.touchCount == 0)
            walkSound.audio.Stop();
        if (direction != Direction.none)
        {
            isMoving = true;
            player.rigidbody.transform.position = Vector3.Lerp(oldPosition, destPosition, 1000f *Time.deltaTime);
            if (player.rigidbody.transform.position == destPosition)
            {
                direction = Direction.none;
                isMoving = false;
            }
        }
	}

}
