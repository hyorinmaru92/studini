﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Assets.Scripts.labirynth
{
    [Serializable]
    public class Size
    {
        public int width;
        public int height;
    }
}
