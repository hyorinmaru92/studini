﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;


public class points : MonoBehaviour {


    private Text score;
    int previousScore=0;
    private FryingPanGameController controller;
    CharSettings settings;

    void Start()
    {
        settings = GameObject.Find("CharSettings").GetComponent<CharSettings>();
        score = GameObject.Find("Score").GetComponent<Text>();
        controller = GameObject.Find("GameController").GetComponent<FryingPanGameController>();
        score.text = previousScore.ToString("00") + "/" + controller.scoreToWin.ToString();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Point")
        {
            
            Destroy(other.gameObject);
            if(settings.Audio)
                audio.Play();
            score.text = (++previousScore).ToString("00") + "/" + controller.scoreToWin.ToString();
            if (previousScore == controller.scoreToWin)
            {
                controller.isEnd = true;
                controller.gameResult = 1;
            }
            this.transform.localScale = new Vector3(this.transform.localScale.x * 1.05f, this.transform.localScale.y*1.02f, this.transform.localScale.z * 1.05f);
        }
    }
}
