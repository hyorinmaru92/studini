﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class FryingPanGameController : MonoBehaviour {

    bool isSet = false;
    float timeFromStart;
    public int timeToLeft=30;
    public int scoreToWin=10;
    public bool isEnd = false;
    public int gameResult = 0;
    public Text time;

    public Transform point;
    public Transform spawns;
    public Transform spawned;

    public void SetTime()
    {
        timeFromStart = Time.time;
        isSet = true;
    }

	void FixedUpdate () {
        if (isSet)
        {
            if (Time.time-timeFromStart > timeToLeft)
            {
                isEnd = true;
                gameResult = -1;
            }
            else
            {
                int min = (int)Mathf.Floor((timeToLeft + timeFromStart - Time.time) / 60);
                int sec = (int)Mathf.Floor((timeToLeft + timeFromStart - Time.time) % 60);
                time.text = min.ToString("00") + ":" + sec.ToString("00");
            }
            if (spawned.childCount == 0)
            {
                Transform newPoint = (Transform)Instantiate(point, spawns.GetChild(Random.Range(0, spawns.childCount)).transform.position, Quaternion.identity);
                newPoint.parent = spawned;
            }
        }
	}
}
