﻿using UnityEngine;
using System.Collections;

public class fry_controller : MonoBehaviour {
    public int force = 10;
    public int tilt = 90;
    float myXaccel = 0.0f;
    float myYaccel = 0.0f;
    float myZaccel = 0.0f;
    public float smoothSpeed = 10f;
	void Start () {
        Input.gyro.enabled = true;
        Input.gyro.updateInterval = 0.01F;
	}
	
	void Update () 
    {
		if (Input.GetKeyDown(KeyCode.Escape)) 
		{
			Application.LoadLevel("kitchen_scene");
		}

        Debug.Log(Input.acceleration);
        //rigidbody.AddForce(Input.gyro.userAcceleration * force);
        Vector3 acceler = Input.acceleration;
        acceler.Normalize();
        myXaccel = Mathf.Lerp(myXaccel, acceler.x, smoothSpeed * Time.deltaTime);
        myYaccel = Mathf.Lerp(myYaccel, acceler.y, smoothSpeed * Time.deltaTime);
        myZaccel = Mathf.Lerp(myZaccel, acceler.z, smoothSpeed * Time.deltaTime);
        //transform.rotation = Quaternion.Euler(-myXaccel * 60 - 90,0 ,-myYaccel * 45 );
        transform.rotation = Quaternion.Euler(-myXaccel * 60, 0, -myYaccel * 45);
        //transform.rotation = Quaternion.Euler(0, 0, 0);
	}
}
