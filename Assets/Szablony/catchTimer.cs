﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Globalization;

public class catchTimer : MonoBehaviour {
	int start;
	public int goal;
	public Text counter;
	private int temp;
    Collect_controller controller;

    public float _timeToGameOver;
    bool isSet = false;
    float timeFromStart = 0;

    public void setDifficulty(int difficult)
    {
        controller = GameObject.Find("Trash").GetComponent<Collect_controller>();
        switch (difficult)
        {
            case 1: _timeToGameOver = 300; break;
            case 2: _timeToGameOver = 200; break;
            case 3: _timeToGameOver = 120; break;
            default:
                break;
        }
        SetTime();
    }

    public void SetTime()
    {
        timeFromStart = Time.time;
        isSet = true;
        controller.isSet = true;
        GameObject.Find("DropPoint").GetComponent<Drop_Controller>().SetDropPoint();
        FindObjectOfType<traps_controller>().SetDropPoint();
    }

	void Start () 
	{
		start = DateTime.Now.Second;
        controller = GameObject.Find("Trash").GetComponent<Collect_controller>();
	}

	void Update () 
	{
        if (isSet)
        {
            if (_timeToGameOver + timeFromStart - Time.time < 0)
            {
                setEndGame();
            }
            int min = (int)Mathf.Floor((_timeToGameOver + timeFromStart - Time.time) / 60);
            int sec = (int)Mathf.Floor((_timeToGameOver + timeFromStart - Time.time) % 60);
            counter.text = min.ToString("00") + ":" + sec.ToString("00");
        }
	}

    private void setEndGame()
    {
        GameObject.Find("Trash").transform.GetComponent<BoxCollider>().isTrigger = false;
        controller.isEnd = true;
    }
}

